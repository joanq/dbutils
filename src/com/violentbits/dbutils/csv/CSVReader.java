package com.violentbits.dbutils.csv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSVReader {
	private static final String SEPARATOR = ";";
	private static final String COMMENT = "#";

	public static List<String[]> read(String filename) throws IOException {
		String line;
		List<String[]> result = new ArrayList<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(filename));) {
			while ((line=reader.readLine())!=null) {
				if (!line.equals("") && !line.startsWith(COMMENT)) {
					result.add(line.split(SEPARATOR));
				}
			}
		}
		return result;
	}
	
	public static List<String> readOne(String filename) throws IOException {
		List<String[]> allDataList = read(filename);
		List<String> firstColList = new ArrayList<String>();
		for (String[] strArray : allDataList) {
			firstColList.add(strArray[0]);
		}
		return firstColList;
	}
}
