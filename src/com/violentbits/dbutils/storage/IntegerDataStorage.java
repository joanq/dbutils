package com.violentbits.dbutils.storage;

import java.util.concurrent.ThreadLocalRandom;

/**
 * An IntegerDataStorage allows to get random integer numbers
 * from a minimum (inclusive) to a maximum (exclusive).
 */
public class IntegerDataStorage implements DataStorage<Integer> {
	/**
	 * Minimum value for number generation
	 */
	public final int min;
	/**
	 * Maximum value for number generation
	 */
	public final int max;
	
	/**
	 * Constructor that receive the minimum and maximum
	 * numbers that defines the rank for this storage.
	 * 
	 * @param min  Minimum generated value (inclusive).
	 * @param max  Maximum generated value (exclusive).
	 */
	public IntegerDataStorage(int min, int max) {
		this.min = min;
		this.max = max;
	}

	/**
	 * Get a random number between min (inclusive) and max (exclusive).
	 * Returns a string with length equal to the length of max-1.
	 * 
	 * @return
	 */
	public String getString() {
		int n = get();
		// Pad with 0 to complete
		int lmax = Integer.toString(max-1).length();
		String s = Integer.toString(n);
		while (s.length() < lmax) {
			s="0"+s;
		}
		return s;
	}

	/**
	 * Get a random number between min (inclusive) and max (exclusive).
	 * 
	 * @return
	 */
	@Override
	public Integer get() {
		return ThreadLocalRandom.current().nextInt(min, max);
	}

}
