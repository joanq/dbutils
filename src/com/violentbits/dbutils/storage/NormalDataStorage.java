package com.violentbits.dbutils.storage;

import java.util.concurrent.ThreadLocalRandom;

public class NormalDataStorage implements DataStorage<Double> {
	public final double mean;
	public final double std_deviation;
	public final double min;
	public final double max;
	public final boolean bounded;
	
	public NormalDataStorage(double mean, double std_deviation) {
		this.mean = mean;
		this.std_deviation = std_deviation;
		min=max=0;
		bounded=false;
	}
	
	public NormalDataStorage(double mean, double std_deviation, double min, double max) {
		this.mean = mean;
		this.std_deviation = std_deviation;
		this.min = min;
		this.max = max;
		bounded = true;
	}

	@Override
	public Double get() {
		double value;
		do {
			value = std_deviation*ThreadLocalRandom.current().nextGaussian()+mean;
		} while (bounded==true && (value<min || value>max));
		return value;
	}

	public long getLong() {
		double n = get();
		return Math.round(n);
	}
}
