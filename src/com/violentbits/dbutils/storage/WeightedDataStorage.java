package com.violentbits.dbutils.storage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

/**
 * This storages stores some values and returns a random
 * value every time get() is called. Each value can
 * have different probability.
 */
public class WeightedDataStorage<T> implements DataStorage<T> {
	/**
	 * List where all values are stored.
	 */
	private List<T> values = new ArrayList<>();
	/**
	 * List where all value frequencies are stored.
	 * Position of each frequency in this list must be the
	 * same as the position of its associated data.
	 */
	private List<Double> freq = new ArrayList<>();
	/**
	 * Sum of all frequencies stored at freq.
	 */
	private double sum;
	
	/**
	 * Constructor that needs a map of values and their associated frequencies.
	 * 
	 * For example, if the possible values are "A", "B", and "C", and "A" appears two
	 * in ten times, "B" appears five in ten times, and "C" appears three in ten times,
	 * the map will have these data:
	 * 
	 * "A" -> 2
	 * "B" -> 5
	 * "C" -> 3
	 * 
	 * @param map
	 */
	public WeightedDataStorage(Map<T, Double> map) {
		for (T key : map.keySet()) {
			values.add(key);
			freq.add(map.get(key));
			sum+=map.get(key);
		}
	}
	
	public WeightedDataStorage(List<T> values, List<Double> freq) {
		this.values = values;
		this.freq = freq;
		for (Double f : freq) {
			sum+=f;
		}
	}

	/**
	 * Returns a stored value, randomly. This respects the probability associated
	 * to each value.
	 */
	@Override
	public T get() {
		double n = ThreadLocalRandom.current().nextDouble(sum);
		double f=0;
		boolean found=false;
		Iterator<T> dataIt = values.iterator();
		Iterator<Double> freqIt = freq.iterator();
		T result=null;
		
		while (!found) {
			f += freqIt.next();
			result = dataIt.next();
			
			if (f>n) {
				found = true;
			}
		}
		return result;
	}

	public List<T> getAll() {
		return Collections.unmodifiableList(values);
	}
}
