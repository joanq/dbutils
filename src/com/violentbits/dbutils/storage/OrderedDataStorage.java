package com.violentbits.dbutils.storage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class OrderedDataStorage<T> implements DataStorage<T> {
	private List<T> values;
	private int index=0;

	@SafeVarargs
	public OrderedDataStorage(T... data) {
		values = new ArrayList<>();
		values.addAll(Arrays.asList(data));
		Collections.shuffle(values);
	}
	
	public OrderedDataStorage(List<T> values) {
		Collections.shuffle(values);
		this.values = values;
	}
	
	@Override
	public T get() {
		T data = values.get(index);
		index=(index+1)%values.size();
		return data;
	}
	
	public List<T> getAll() {
		return Collections.unmodifiableList(values);
	}
}
