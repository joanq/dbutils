package com.violentbits.dbutils.storage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * This storage returns random value taken from all possible
 * value. Each value has the same probability.
 */
public class UniformDataStorage<T> implements DataStorage<T> {
	/**
	 * List where all possible values that this storage can give are stored.
	 */
	private List<T> values = new ArrayList<>();

	/**
	 * Constructor that receives some values inside an array.
	 * 
	 * @param data
	 */
	@SafeVarargs
	public UniformDataStorage(T... data) {
		values.addAll(Arrays.asList(data));
	}
	/**
	 * Constructor that receive a collection with all possible values.
	 * 
	 * @param col
	 */
	public UniformDataStorage(Collection<? extends T> col) {
		values.addAll(col);
	}
	
	public UniformDataStorage(List<T> values) {
		this.values = values;
	}
	
	/**
	 * Returns a random value from all that this storage has.
	 * 
	 * @return
	 */
	@Override
	public T get() {
		return values.get(ThreadLocalRandom.current().nextInt(values.size()));
	}
	
	public List<T> getAll() {
		return Collections.unmodifiableList(values);
	}
}
