package com.violentbits.dbutils.storage;

/**
 * Classes that implement DataStorage store information
 * and select some random data every time the method
 * get() is called.
 */
public interface DataStorage<T> {
	/**
	 * Returns a random piece of data taken from
	 * the stored information.
	 * 
	 * @return
	 */
	public T get();
}
